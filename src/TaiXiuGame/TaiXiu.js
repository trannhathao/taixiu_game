import React, { Component } from "react";
import { connect } from "react-redux";
import { datCuocAction, playGameAction } from "./Redux/actions/xucXacActions";
import "./TaiXiu.css";
import ThongTinTroChoi from "./ThongTinTroChoi";
import XucXac from "./XucXac";
class TaiXiu extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center display-4 mt-1">TÀI XỈU</div>
        <div className="row text-center mt-2">
          <div className="col-4">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(true);
              }}
            >
              TÀI
            </button>
          </div>
          <div className="col-4">
            <XucXac />
          </div>
          <div className="col-4">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(false);
              }}
            >
              XỈU
            </button>
          </div>
        </div>
        <div className="thongTinTroChoi text-center">
          <ThongTinTroChoi />
          <button
            className="btnPlay"
            onClick={() => {
              this.props.playGame();
            }}
          >
            Play game
          </button>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      //gửi lên reducer
      dispatch(datCuocAction(taiXiu));
    },
    playGame: () => {
      dispatch(playGameAction());
    },
  };
};
export default connect(null, mapDispatchToProps)(TaiXiu);
