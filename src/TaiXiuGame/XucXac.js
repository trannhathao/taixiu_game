import React, { Component } from "react";
import { connect } from "react-redux";

class XucXac extends Component {
  renderXucXac = () => {
    // Lấy props từ reducer về
    return this.props.mangXucXac.map((xucXac, index) => {
      return (
        <img
          key={index}
          className="hinhanh-xucxac ml-2"
          // style={{ width: 100, height: 100 }}
          src={xucXac.hinhAnh}
          alt="hinhAnh"
        />
      );
    });
  };
  render() {
    return <div>{this.renderXucXac()}</div>;
  }
}
// Hàm lấy state từ redux về thành props của component
let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};
export default connect(mapStateToProps)(XucXac);
