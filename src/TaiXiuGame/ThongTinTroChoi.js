import React, { Component } from "react";
import { connect } from "react-redux";

class ThongTinTroChoi extends Component {
  render() {
    return (
      <div className="txt-thongtin-trochoi">
        <div>
          Bạn chọn:{" "}
          <span className="text-danger">
            {this.props.taiXiu ? "TÀI" : "XỈU"}
          </span>
        </div>
        <div>
          Bàn thắng:{" "}
          <span className="text-success">{this.props.soBanThang}</span>
        </div>
        <div>
          Tổng số bàn chơi:{" "}
          <span className="text-primary">{this.props.tongSoBanChoi}</span>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    taiXiu: state.xucXacReducer.taiXiu,
    soBanThang: state.xucXacReducer.soBanThang,
    tongSoBanChoi: state.xucXacReducer.tongSoBanChoi,
  };
};
export default connect(mapStateToProps)(ThongTinTroChoi);
