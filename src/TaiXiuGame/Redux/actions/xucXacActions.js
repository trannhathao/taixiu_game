import { DAT_CUOC, PLAY_GAME } from "../constant/xucXacConstant";

export const datCuocAction = (taiXiu) => {
  return {
    type: DAT_CUOC,
    taiXiu,
  };
};

export const playGameAction = () => {
  return {
    type: PLAY_GAME,
  };
};
